﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using migrations.Models;
using Microsoft.Extensions.Configuration;


namespace migrations
{
    public class ApplicationDbContext:DbContext
    {
        public DbSet<Manufacturer> Manufacturers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var bilder = new ConfigurationBuilder();
            bilder.SetBasePath(@"D:\vs\migrations\migrations\migrations");
            bilder.AddJsonFile("appsetings.json");
            var config = bilder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }


    }

}
