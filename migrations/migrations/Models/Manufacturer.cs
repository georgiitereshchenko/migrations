﻿using migrations.Enums;
using migrations.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace migrations.Models
{
    public class Manufacturer : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public int FundYear { get; set; }
    }
}
